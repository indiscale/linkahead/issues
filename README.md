# LinkAhead Issues

[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://docs.indiscale.com/caosdb-deploy/)

Hello! This repository serves the sole purpose of collecting and keeping track
of issues, bugs, and feature request of
[LinkAhead](https://www.indiscale.com/linkahead/), your open-source
research-data management system. It does not and is not meant to contain the
codebase of LinkAhead. Please note that in the end, LinkAhead is just a CaosDB
distribution, the code of which is publicly available on
[gitlab.com/caosdb](https://gitlab.com/caosdb).

## Report an issue

If you encounter a bug, want to submit a feature request, or find an error in
the [documentation](https://docs.indiscale.com/caosdb-deploy/), please [open an
issue](https://gitlab.com/indiscale/linkahead/issues/-/issues/new) in this
repository. Please describe your problem or idea as precise as possible, but
remember that an incomplete issue is still better than no issue at all.

## Where to get LinkAhead

You can download the single-user trial version on
https://www.indiscale.com/download/ as a Debian package. If you're interested in
your individual data-management solution, don't hesitate to
[contact](https://www.indiscale.com/#jumpcontact) IndiScale.

## Join the community!

There is a CaosDB community channel on matrix: https://matrix.to/#/#caosdb:matrix.org

You can also always contact us directly via info@indiscale.com

## License

LinkAhead is licensed under the GNU Affero Public License, version 3, see
[LICENSE.md](LICENSE.md).
